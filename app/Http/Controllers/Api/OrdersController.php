<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Mongodb\Order;
use Validator;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class OrdersController extends Controller
{
    public function index()
    {
        $order_list = Order::all()->toArray();
        return response([
            'status'    => true,
            'message'   => 'Orders list',
            'data'      => $order_list,
        ], 200);
    }

    public function store(Request $request)
    {
        if($request->hasFile('booking_files')){
            $file       = $request->file('booking_files');
            $ext        = $file->getClientOriginalExtension();

            if ($ext != "csv") {
                return response([
                    'status'    => false,
                    'message'   => 'Please upload only .CSV file',
                ], 402);
            }

            $header = null;
            $data = array();
            $is_data_inserted = array();
            if (($handle = fopen($file, 'r')) !== false){
                while (($row = fgetcsv($handle, 1000, ',')) !== false){
                    if (!$header){
                        $header = $row;
                    }
                    else{
                        $data[] = array_combine($header, $row);
                        if(count($data) >= 5){
                            $result             = $this->validate_request_and_then_insert($data);
                            $is_data_inserted[] = $result; /*To check is any data is uploaded in db*/
                            unset($data);
                        }
                    }
                }
                fclose($handle);

                /*For uploading pending value of $data variable in database*/
                if(count($data) > 0){
                    $result             = $this->validate_request_and_then_insert($data);
                    $is_data_inserted[] = $result; /*To check is any data is uploaded in db*/
                    unset($data);
                }
                
                if(in_array(1, $is_data_inserted)){
                    return response([
                        'status'    => true,
                        'message'   => 'File imported successfully',
                    ], 200);
                }
                else{
                    return response([
                        'status'    => false,
                        'message'   => 'Failed to import data. File might be empty or have duplicate values',
                    ], 402);
                }
            }
            else{
                return response([
                    'status'    => false,
                    'message'   => 'Failed to read CSV file',
                ], 402);
            }
        }
        else{
            return response([
                'status'    => false,
                'message'   => 'Please upload a CSV file',
            ], 402);
        }
    }
    
    private function validate_request_and_then_insert($data)
    {   
        foreach ($data as $key => $value) {
            $validator = Validator::make($value, $this->rules());
            if ($validator->fails()) {
                Log::channel('booking_validation_failed_entry')->info('validation_failed_data', [
                    'data' => json_encode($value),
                    'message' => $validator->messages(),
                ]);
                unset($data[$key]);
            }
        }

        $data               = array_values($data); /*Re-arranging array indexes from 0 position*/
        $create_order       = (count($data) > 0) ? Order::insert($data) : 0;
        return $create_order;
    }

    private function rules()
    {
        return array(
            'tenant_id'                         => 'required|numeric',
            'airwaybilno'                       => 'required|unique:mongodb.orders,airwaybilno',
            'transaction_id'                    => 'required|unique:mongodb.orders,transaction_id',
            'orderno'                           => 'required|unique:mongodb.orders,orderno',
            'courier_courier_code'              => 'required',
            
            'consignee_firstname'               => 'required',
            'consignee_address1'                => 'required',
            'consignee_city'                    => 'required',
            'consignee_pincode'                 => 'required',
            'consignee_state'                   => 'required',
            'consignee_telephone1'              => 'required',
            
            'consignor_name'                    => 'required',
            'consignor_sub_vendor_name'         => 'required',
            'consignor_sub_vendor_address1'     => 'required',
            'consignor_sub_vendor_phone'        => 'required',
            'consignor_sub_vendor_pincode'      => 'required',
            'consignor_sub_vendor_city'         => 'required',
            'consignor_sub_vendor_state'        => 'required',
            
            'consignor_rto_name'                => 'required',
            'consignor_rto_address1'            => 'required',
            'consignor_rto_phone'               => 'required',
            'consignor_rto_pincode'             => 'required',
            'consignor_rto_city'                => 'required',
            'consignor_rto_state'               => 'required',
            
            'packages_description'              => 'required',
            'packages_quantity'                 => 'required|numeric',
            'packages_price'                    => 'required|numeric',

            'payment_paytype'                   => 'required',
            'payment_collectable'               => 'required|numeric',
            'payment_invoice_amount'            => 'required|numeric',
            'payment_discount'                  => 'required',
            'payment_actual_weight'             => 'required',
            'payment_volumetric_weight'         => 'required',
            'payment_length'                    => 'required',
            'payment_breadth'                   => 'required',
            'payment_height'                    => 'required',
            'payment_shipping_charges'          => 'required',            
        );
    }
}
