<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Models\Mysql\User;
use Validator;
use Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_list = User::where('deleted_at', NULL)->get();
        return response([
            'status'    => true,
            'message'   => 'Users list',
            'data'      => $user_list,
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|min:2|max:100',
            'email'     => 'required|email|unique:users,email',
            'mobile'    => 'required|digits:10|unique:users,mobile',
            'password'  => 'required|min:6|max:10',
        ]);
        
        if ($validator->fails()) {
            return response($validator->messages(), 402);
        }

        $data               = $request->all();
        $data['password']   = Hash::make($data['password']);
        $create_user        = User::create($data);

        if(!$create_user){
            return response([
                'status'    => false,
                'message'   => 'Failed to insert data',
                'data'      => '',
            ], 402);
        }
        else{
            return response([
                'status'    => true,
                'message'   => 'Record inserted successfully',
                'data'      => $create_user,
            ], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $array = ['products' => ['desk' => ['price' => ['quantity' => 101, 'demo' => 1000]]]];
        $price = Arr::get($array, 'products.desk.price.demo');
        dd($price);
        $user_data  = User::find($request->id);
        if(!$user_data){
            return response([
                'status' => false,
                'msg' => 'User does not exist'
            ], 402);
        }
        else{
            if($user_data->delete()){
                return response([
                    'status'    => true,
                    'message'   => 'User deleted successfully',
                ], 200);
            }
            else{
                return response([
                    'status'    => false,
                    'message'   => 'Failed to delete user',
                ], 200);
            }
        }
    }
}
