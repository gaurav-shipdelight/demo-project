<?php

namespace App\Models\Mysql;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;
    protected $softDelete = true;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name', 'email', 'mobile', 'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password', 'remember_token', 'deleted_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /*Accessor to customise the column values of a DB-table*/
    public function getCreatedAtAttribute($value){
        return $value = date('Y-m-d H:i:s', strtotime($value));
    }

    public function getUpdatedAtAttribute($value){
        return $value = date('Y-m-d H:i:s', strtotime($value));
    }

    /*Mutator to set the incoming request values accordingly and then inserting in DB-table*/
    public function setNameAttribute($value){
        $this->attributes['name'] = ucfirst(strtolower($value));
    }

    public function setEmailAttribute($value){
        $this->attributes['email'] = strtolower($value);
    }
}
