<?php

namespace App\Models\Mongodb;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Order extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'orders';
    protected $primaryKey = '_id';
    protected $guarded    = ['_id'];
    public $timestamps    = false;

    protected $fillable = [
        'airwaybilno', 'tenant_id', 'transaction_id', 'orderno', 'servicetype', 'channel', 'courier', 'consignee', 'consignor', 'packages', 'payment'
    ];
}
