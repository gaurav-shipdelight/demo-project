<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\OrdersController;
use App\Http\Controllers\Api\TestController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/*Users section*/
Route::get('show_user', [UsersController::class, 'index'])->name('user.show');
Route::post('insert_user', [UsersController::class, 'store'])->name('user.insert');
Route::post('delete_user', [UsersController::class, 'destroy'])->name('user.delete');

/*Orders Section*/
Route::get('show_order', [OrdersController::class, 'index'])->name('order.show');
Route::post('insert_order', [OrdersController::class, 'store'])->name('order.insert');

/*Testing Section*/
Route::get('testing',  [TestController::class, 'testing_play'])->name('test.testing_play');
